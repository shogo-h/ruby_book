def fizz_buzz(i)
  if i % 3 == 0 && i % 5 == 0
    'Fizz Buzz'
  elsif i % 3 == 0
    'Fizz'
  elsif i % 5 == 0
    'Buzz'
  else
    i.to_s
  end
end
